﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CuaHangSach.Models
{
    public class ThanhToanViewModel
    {
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        public string HoTenKH { get; set; }
        [EmailAddress(ErrorMessage = "Có định dạng không hợp lệ!")]
        public string EmailKH { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        public string DiaChiKH { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        public string DienThoaiKH { get; set; }
    }
}