﻿using CuaHangSach.Databases;
using CuaHangSach.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace CuaHangSach.Controllers
{
    public class TaiKhoanController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        [HttpPost]
        public ActionResult KiemTraDangNhap(DangNhapViewModel nguoidung)
        {
            if (ModelState.IsValid)
            {
                //cấu trúc linq kiểm tra xem có tài khoản nào khớp hay không
                var CheckDangNhap = db.NGUOIDUNGs.Any(s => s.TaiKhoanND == nguoidung.TaiKhoanND && s.MatKhauND == nguoidung.MatKhauND);
                //nếu có checklogin = true
                if (CheckDangNhap)
                {
                    FormsAuthentication.SetAuthCookie(nguoidung.TaiKhoanND, false);
                    return RedirectToAction("DangNhapThanhCong");
                }
                else
                {
                    ModelState.AddModelError("", "Sai tên tài khoản hoặc mật khẩu!");
                    //trở lại trang đăng nhập, báo lỗi
                    return View("DangNhap", nguoidung);
                }
            }
            //Đăng nhập thành công, trở về trang chủ
            return Redirect("/");
        }
        [HttpPost]
        public ActionResult LuuDangKy([Bind(Include = "HoTenND, TaiKhoanND, MatKhauND, EmailND, DiaChiND, DienThoaiND, NgaySinhND, GioiTinhND")]NGUOIDUNG nguoidung)
        {
            //Kiểm tra hợp lệ dữ liệu
            if (ModelState.IsValid)
            {
                //kiểm tra tên đăng nhập, email có bị đã tồn tại hay chưa?
                var CheckTaiKhoanND = db.NGUOIDUNGs.Any(s => s.TaiKhoanND == nguoidung.TaiKhoanND);
                var CheckEmailND = db.NGUOIDUNGs.Any(s => s.EmailND == nguoidung.EmailND);
                //Các lỗi nếu có trong quá trình đăng ký tài khoản
                if (CheckTaiKhoanND)
                {
                    ModelState.AddModelError("", "Tên tài khoản đã tồn tại!");
                }
                if (CheckEmailND)
                {
                    ModelState.AddModelError("", "Email đã có người sử dụng!");
                }
                if (CheckTaiKhoanND == true || CheckEmailND == true)
                {
                    //trả về view đăng ký và thông báo các lỗi ở trên
                    return View("DangKy");
                }
                else
                {
                    //Lưu thông tin tài khoản vào CSDL
                    db.NGUOIDUNGs.Add(nguoidung);
                    db.SaveChanges();
                    //xác thực tài khoản trong ứng dụng
                    FormsAuthentication.SetAuthCookie(nguoidung.TaiKhoanND, false);
                    //trả về trang chủ đăng ký thành công
                    return Redirect("DangKyThanhCong");
                }
            }
            else
            {
                //Trang báo lỗi nhập liệu không hợp lệ!
                return View("Error");
            }
        }
        public ActionResult DangNhap()
        {
            return View();
        }
        public ActionResult DangKy()
        {
            return View();
        }
        public ActionResult DangXuat(DangNhapViewModel model)
        {
            //Đăng xuất khỏi ứng dụng
            FormsAuthentication.SignOut();
            return RedirectToAction("DangNhap", new { GhiNho = model.GhiNho });
        }
        public ActionResult HeThongQuanLy()
        {
            return View();
        }
        public ActionResult DangNhapThanhCong()
        {
            return View();
        }
        public ActionResult DangKyThanhCong()
        {
            return View();
        }
    }
}