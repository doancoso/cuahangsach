﻿using CuaHangSach.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class QuanLyChiTietDonHangController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult ChiTietDonHang()
        {
            var ListChiTietDonHang = db.CHITIETDONHANGs.ToList();
            if (ListChiTietDonHang.Count > 0)
            {
                return View(ListChiTietDonHang);
            }
            else
            {
                return RedirectToAction("ChiTietDonHangRong");
            }
        }
        public ActionResult XoaChiTietDonHang(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CHITIETDONHANG chitietdonhang = db.CHITIETDONHANGs.Find(id);
            if (chitietdonhang == null)
            {
                return HttpNotFound();
            }
            return View(chitietdonhang);
        }
        [HttpPost, ActionName("XoaChiTietDonHang")]
        [ValidateAntiForgeryToken]
        public ActionResult XoaChiTietDonHang(int id)
        {
            CHITIETDONHANG chitietdonhang = db.CHITIETDONHANGs.Find(id);
            db.CHITIETDONHANGs.Remove(chitietdonhang);
            db.SaveChanges();
            return RedirectToAction("ChiTietDonHang");
        }
        public ActionResult ChiTietDonHangRong()
        {
            return View();
        }
    }
}