﻿using CuaHangSach.Databases;
using CuaHangSach.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Controllers
{
    public class QuanLyCuaHangController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult CuaHang(int? page)
        {
            var ListSach = db.SACHes.ToList();
            int PageNumber = (page ?? 1);
            int PageSize = 8;
            return View(ListSach.ToPagedList(PageNumber, PageSize));
        }
        public ActionResult TimKiemSach(string search)
        {
            var ListSach = from s in db.SACHes select s;
            if (!String.IsNullOrEmpty(search))
            {
                ListSach = db.SACHes.Where(s => s.TenSach.Contains(search));
            }
            else
            {
                return RedirectToAction("Oops", "Order");
            }
            ViewBag.SearchTerm = search;
            return View(ListSach.ToList());
        }
        public ActionResult ThongTinSach(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            SACH sach = db.SACHes.Find(id);
            return View(sach);
        }
    }
}