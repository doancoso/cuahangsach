﻿using CuaHangSach.Databases;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class QuanLyDonHangController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult DonHang()
        {
            var ListDonHang = db.DONDATHANGs.ToList();
            if (ListDonHang.Count > 0)
            {
                return View(ListDonHang);
            }
            else
            {
                return RedirectToAction("DonHangRong");
            }
        }
        public ActionResult ChinhSuaDonHang(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DONDATHANG dondathang = db.DONDATHANGs.SingleOrDefault(s => s.MaDDH == id);
            return View(dondathang);
        }
        [HttpPost, ActionName("ChinhSuaDonHang")]
        [ValidateAntiForgeryToken]
        public ActionResult ChinhSuaDonHang(int id)
        {
            var dondathangUpdate = db.DONDATHANGs.Find(id);
            if (TryUpdateModel(dondathangUpdate, "", new string[] { "HoTenKH", "DienThoaiKH", "EmailKH", "DiaChiKH", "DaThanhToan", "TinhTrangGiaoHang", "NgayGiao" }))
            {
                try
                {
                    db.Entry(dondathangUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Error Save Data");
                }
            }
            return RedirectToAction("DonHang");
        }
        public ActionResult XoaDonHang(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DONDATHANG dondathang = db.DONDATHANGs.Find(id);
            if (dondathang == null)
            {
                return HttpNotFound();
            }
            return View(dondathang);
        }
        [HttpPost, ActionName("XoaDonHang")]
        [ValidateAntiForgeryToken]
        public ActionResult XoaDonHang(int id)
        {
            DONDATHANG dondathang = db.DONDATHANGs.Find(id);
            db.DONDATHANGs.Remove(dondathang);
            db.SaveChanges();
            return RedirectToAction("DonHang");
        }
        public ActionResult DonHangRong()
        {
            return View();
        }
    }
}