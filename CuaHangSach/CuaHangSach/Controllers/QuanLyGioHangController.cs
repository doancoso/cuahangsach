﻿using CuaHangSach.Databases;
using CuaHangSach.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CuaHangSach.Controllers
{
    public class QuanLyGioHangController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        [HttpPost]
        public JsonResult ThemCartItem(int id)
        {
            List<CartItem> ListGioHang;
            if (Session["GioHang"] == null)
            {
                ListGioHang = new List<CartItem>();
                ListGioHang.Add(new CartItem { Quality = 1, ProductOrder = db.SACHes.Find(id) });
                Session["GioHang"] = ListGioHang;
            }
            else
            {
                bool flag = false;
                ListGioHang = (List<CartItem>)Session["GioHang"];
                foreach (CartItem item in ListGioHang)
                {
                    if (item.ProductOrder.MaSach == id)
                    {
                        item.Quality++;
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    ListGioHang.Add(new CartItem { Quality = 1, ProductOrder = db.SACHes.Find(id) });
                Session["GioHang"] = ListGioHang;
            }
            int cartcount = 0;
            List<CartItem> ls = (List<CartItem>)Session["GioHang"];
            foreach (CartItem item in ls)
            {
                cartcount += item.Quality;
            }
            return Json(new { ItemAmount = cartcount });
        }
        public ActionResult CapNhatCartItem(FormCollection Fc)
        {
            string[] Quantity = Fc.GetValues("Quality");
            List<CartItem> ListGioHang = (List<CartItem>)Session["GioHang"];
            for (int i = 0; i < ListGioHang.Count; i++)
                ListGioHang[i].Quality = Convert.ToInt32(Quantity[i]);
            Session["GioHang"] = ListGioHang;
            return Redirect("GioHang");
        }
        public JsonResult XoaCartItem(int id)
        {
            var ListGioHang = (List<CartItem>)Session["GioHang"];
            ListGioHang.RemoveAll(s => s.ProductOrder.MaSach == id);
            Session["GioHang"] = ListGioHang;
            return Json(new { status = true });
        }
        public ActionResult ThanhToan()
        {
            var ListCart = Session["GioHang"];
            var ListGioHang = new List<CartItem>();
            if (ListCart != null)
            {
                ListGioHang = (List<CartItem>)ListCart;
            }
            return View(ListGioHang);
        }
        [HttpPost]
        public ActionResult ThanhToan(string HoTenKH, string EmailKH, string DiaChiKH, string DienThoaiKH)
        {
            var dondathang = new DONDATHANG();
            dondathang.HoTenKH = HoTenKH;
            dondathang.EmailKH = EmailKH;
            dondathang.DiaChiKH = DiaChiKH;
            dondathang.DienThoaiKH = DienThoaiKH;
            dondathang.NgayDat = DateTime.Now;

            try
            {
                var ID = new DonDatHangModel().Insert(dondathang);
                var ListGioHang = (List<CartItem>)Session["GioHang"];
                var DonHangModel = new ChiTietDonHangModel();
                foreach (var item in ListGioHang)
                {
                    var chitietdonhang = new CHITIETDONHANG();
                    chitietdonhang.MaCTDH = ID;
                    chitietdonhang.MaDDH = dondathang.MaDDH;
                    chitietdonhang.MaSach = item.ProductOrder.MaSach;
                    chitietdonhang.DonGia = item.ProductOrder.GiaBan * item.Quality;
                    chitietdonhang.SoLuong = item.Quality;
                    DonHangModel.Insert(chitietdonhang);
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Oops", "Order");
            }
            return Redirect("HoanThanh");
        }
        public ActionResult GioHang()
        {
            if (Session.Count > 0)
            {
                return View();
            }
            else
            {
                return RedirectToAction("GioHangRong");
            }
        }
        public ActionResult GioHangRong()
        {
            return View();
        }
        public ActionResult HoanThanh()
        {
            return View();
        }
    }
}