﻿using CuaHangSach.Databases;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class QuanLyNhaXuatBanController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult NhaXuatBan(int? page)
        {
            var ListNhaXuatBan = db.NHAXUATBANs.ToList();
            int PageNumber = (page ?? 1);
            int PageSize = 6;
            return View(ListNhaXuatBan.ToPagedList(PageNumber, PageSize));
        }
        public ActionResult ChinhSuaNhaXuatBan(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NHAXUATBAN nhaxuatban = db.NHAXUATBANs.SingleOrDefault(s => s.MaNXB == id);
            return View(nhaxuatban);
        }
        [HttpPost, ActionName("ChinhSuaNhaXuatBan")]
        [ValidateAntiForgeryToken]
        public ActionResult ChinhSuaNhaXuatBan(int id)
        {
            var nhaxuatbanUpdate = db.NHAXUATBANs.Find(id);
            if (TryUpdateModel(nhaxuatbanUpdate, "", new string[] { "TenNXB", "DiaChiNXB", "DienThoaiNXB" }))
            {
                try
                {
                    db.Entry(nhaxuatbanUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Error Save Data");
                }
            }
            return RedirectToAction("NhaXuatBan");
        }
        public ActionResult XoaNhaXuatBan(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NHAXUATBAN nhaxuatban = db.NHAXUATBANs.Find(id);
            if (nhaxuatban == null)
            {
                return HttpNotFound();
            }
            return View(nhaxuatban);
        }
        [HttpPost, ActionName("XoaNhaXuatBan")]
        [ValidateAntiForgeryToken]
        public ActionResult XoaNhaXuatBan(int id)
        {
            NHAXUATBAN nhaxuatban = db.NHAXUATBANs.Find(id);
            db.NHAXUATBANs.Remove(nhaxuatban);
            db.SaveChanges();
            return RedirectToAction("NhaXuatBan");
        }
        [HttpPost, ActionName("ThemNhaXuatBan")]
        public ActionResult ThemNhaXuatBan([Bind(Include = "TenNXB, DiaChiNXB, DienThoaiNXB")]NHAXUATBAN nhaxuatban)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.NHAXUATBANs.Add(nhaxuatban);
                    db.SaveChanges();
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Error Save Data");
            }
            return RedirectToAction("NhaXuatBan");
        }
        public ActionResult ThemNhaXuatBan()
        {
            return View();
        }
    }
}