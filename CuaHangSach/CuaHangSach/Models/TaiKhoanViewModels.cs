﻿using CuaHangSach.Databases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace CuaHangSach.Models
{
    public class DangNhapViewModel
    {
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        public string TaiKhoanND { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        [DataType(DataType.Password)]
        public string MatKhauND { get; set; }
        public bool GhiNho { get; set; }
    }
    public class DangKyViewModel
    {
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        public string HoTenND { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        [StringLength(12, ErrorMessage = "Tài khoản tối đa 12 ký tự!")]
        public string TaiKhoanND { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        [StringLength(15, ErrorMessage = "Mật khẩu tối đa 15 ký tự!")]
        [DataType(DataType.Password)]
        public string MatKhauND { get; set; }
        [DataType(DataType.Password)]
        [Compare("MatKhauND", ErrorMessage = "Mật khẩu không trùng khớp!")]
        public string XacNhanMatKhauND { get; set; }
        [Required(ErrorMessage = "Không được bỏ trống mục này!")]
        [EmailAddress(ErrorMessage = "Có định dạng không hợp lệ!")]
        public string EmailND { get; set; }
        public string DiaChiND { get; set; }
        public string DienThoaiND { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string NgaySinhND { get; set; }
        public Nullable<bool> GioiTinhND { get; set; }
    }
    public class PhanQuyenProvider : RoleProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] GetRolesForUser(string tendangnhap)
        {
            using (QLBANSACHEntities db = new QLBANSACHEntities())
            {
                var taikhoan = db.NGUOIDUNGs.FirstOrDefault(s => s.TaiKhoanND == tendangnhap);
                if (taikhoan == null)
                {
                    return null;
                }
                else
                {
                    string[] ListQuyen = taikhoan.QUYENs.Select(x => x.TenQuyen).ToArray();
                    return ListQuyen;
                }
            }
        }
        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}