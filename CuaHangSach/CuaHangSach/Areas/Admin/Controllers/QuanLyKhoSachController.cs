﻿using CuaHangSach.Databases;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class QuanLyKhoSachController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult KhoSach(int? page)
        {
            var ListSach = db.SACHes.ToList();
            int PageNumber = (page ?? 1);
            int PageSize = 9;
            return View(ListSach.ToPagedList(PageNumber, PageSize));
        }
        public ActionResult ChinhSuaSach(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SACH sach = db.SACHes.SingleOrDefault(s => s.MaSach == id);
            return View(sach);
        }
        [HttpPost, ActionName("ChinhSuaSach")]
        [ValidateAntiForgeryToken]
        public ActionResult ChinhSuaSach(int id)
        {
            var sachUpdate = db.SACHes.Find(id);
            if (TryUpdateModel(sachUpdate, "", new string[] { "AnhBia", "TenSach", "MaCD", "MaNXB", "MoTa", "SoLuongTon", "GiaBan" }))
            {
                try
                {
                    db.Entry(sachUpdate).State = EntityState.Modified;
                    sachUpdate.NgayCapNhat = DateTime.Now;
                    db.SaveChanges();
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Error Save Data");
                }
            }
            return RedirectToAction("KhoSach");
        }
        public ActionResult XoaSach(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SACH sach = db.SACHes.Find(id);
            if (sach == null)
            {
                return HttpNotFound();
            }
            return View(sach);
        }
        [HttpPost, ActionName("XoaSach")]
        [ValidateAntiForgeryToken]
        public ActionResult XoaSach(int id)
        {
            SACH sach = db.SACHes.Find(id);
            db.SACHes.Remove(sach);
            db.SaveChanges();
            return RedirectToAction("KhoSach");
        }
        [HttpPost, ActionName("ThemSach")]
        public ActionResult ThemSach([Bind(Include = "AnhBia, TenSach, MaCD, MaNXB, MoTa, SoLuongTon, GiaBan")]SACH sach)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.SACHes.Add(sach);
                    sach.NgayCapNhat = DateTime.Now;
                    db.SaveChanges();
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Error Save Data");
            }
            return RedirectToAction("KhoSach");
        }
        public ActionResult ThemSach()
        {
            return View();
        }
    }
}