﻿using CuaHangSach.Databases;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class QuanLyNguoiDungController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult NguoiDung(int? page)
        {
            var ListNguoiDung = db.NGUOIDUNGs.ToList();
            int PageNumber = (page ?? 1);
            int PageSize = 3;
            return View(ListNguoiDung.ToPagedList(PageNumber, PageSize));
        }
        public ActionResult ChinhSuaNguoiDung(int id)
        {
            var ListNguoiDung = db.NGUOIDUNGs.Find(id);
            var ListQuyen = (from s in db.QUYENs select new { s.MaQuyen, s.TenQuyen }).ToList();
            ListQuyen.Add(new { MaQuyen = -1, TenQuyen = "---" });
            var x = ListQuyen.Select(c => new SelectListItem
            {
                Text = c.TenQuyen,
                Value = c.MaQuyen.ToString(),
                Selected = (c.MaQuyen == -1)
            }).ToList();
            ViewBag.ListQuyen = x;
            return View(ListNguoiDung);
        }
        [HttpPost]
        public ActionResult LuuChinhSuaNguoiDung(int MaND, int MaQuyen)
        {
            var ListNguoiDung = db.NGUOIDUNGs.Find(MaND);
            if (TryUpdateModel(ListNguoiDung, "", new string[] { "HoTenND", "TaiKhoanND", "MatKhauND", "EmailND", "DiaChiND", "DienThoaiND", "NgaySinhND", "GioiTinh" }))
            {
                db.Entry(ListNguoiDung).State = System.Data.Entity.EntityState.Modified;
                if (MaQuyen != -1)
                {
                    var ListQuyen = db.QUYENs.Find(MaQuyen);
                    ListNguoiDung.QUYENs.Add(ListQuyen);
                }
                db.SaveChanges();
            }
            return RedirectToAction("NguoiDung");
        }
    }
}