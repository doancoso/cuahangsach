﻿using CuaHangSach.Databases;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace CuaHangSach.Areas.Admin.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class QuanLyChuDeController : Controller
    {
        QLBANSACHEntities db = new QLBANSACHEntities();
        public ActionResult ChuDe(int? page)
        {
            var ListChuDe = db.CHUDEs.ToList();
            int PageNumber = (page ?? 1);
            int PageSize = 6;
            return View(ListChuDe.ToPagedList(PageNumber, PageSize));
        }
        public ActionResult ChinhSuaChuDe(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CHUDE chude = db.CHUDEs.SingleOrDefault(s => s.MaCD == id);
            return View(chude);
        }
        [HttpPost, ActionName("ChinhSuaChuDe")]
        [ValidateAntiForgeryToken]
        public ActionResult ChinhSuaChuDe(int id)
        {
            var chudeUpdate = db.CHUDEs.Find(id);
            if (TryUpdateModel(chudeUpdate, "", new string[] { "TenCD" }))
            {
                try
                {
                    db.Entry(chudeUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (RetryLimitExceededException)
                {
                    ModelState.AddModelError("", "Error Save Data");
                }
            }
            return RedirectToAction("ChuDe");
        }
        public ActionResult XoaChuDe(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CHUDE chude = db.CHUDEs.Find(id);
            if (chude == null)
            {
                return HttpNotFound();
            }
            return View(chude);
        }
        [HttpPost, ActionName("XoaChuDe")]
        [ValidateAntiForgeryToken]
        public ActionResult XoaChuDe(int id)
        {
            CHUDE chude = db.CHUDEs.Find(id);
            db.CHUDEs.Remove(chude);
            db.SaveChanges();
            return RedirectToAction("ChuDe");
        }
        [HttpPost, ActionName("ThemChuDe")]
        public ActionResult ThemChuDe([Bind(Include = "TenCD")]CHUDE chude)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.CHUDEs.Add(chude);
                    db.SaveChanges();
                }
            }
            catch (RetryLimitExceededException)
            {
                ModelState.AddModelError("", "Error Save Data");
            }
            return RedirectToAction("ChuDe");
        }
        public ActionResult ThemChuDe()
        {
            return View();
        }
    }
}