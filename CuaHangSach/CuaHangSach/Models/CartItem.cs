﻿using CuaHangSach.Databases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CuaHangSach.Models
{
    public class CartItem
    {
        public SACH ProductOrder { get; set; }
        public DONDATHANG ProductDDH { get; set; }
        public CHITIETDONHANG ProductCTDH { get; set; }
        public int Quality { get; set; }
    }
    public class DonDatHangModel
    {
        QLBANSACHEntities db = null;
        public DonDatHangModel()
        {
            db = new QLBANSACHEntities();
        }
        public int Insert(DONDATHANG dondathang)
        {
            db.DONDATHANGs.Add(dondathang);
            db.SaveChanges();
            return dondathang.MaDDH;
        }
    }
    public class ChiTietDonHangModel
    {
        QLBANSACHEntities db = null;
        public ChiTietDonHangModel()
        {
            db = new QLBANSACHEntities();
        }
        public bool Insert(CHITIETDONHANG chitietdonhang)
        {
            try
            {
                db.CHITIETDONHANGs.Add(chitietdonhang);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}